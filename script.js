Vue.component('task', {
  props: ['id', 'text', 'count'],
  template: '<div class="sticks"><i style="display: none" class=" next far fa-hand-point-right fa-2x"></i><h3>Задача №{{id}}</h3><div class="wrap-for-text"><textarea  disabled>{{text}}</textarea><div class="icons"><i class="far fa-trash-alt fa-2x" @click="removeTask(count)"></i><i class="far fa-edit fa-2x" @click="changeTask(count)" ></i></div><div class="icons-mobile"><i class="far fa-trash-alt fa-3x" @click="removeTask(count)"></i><i class="far fa-edit fa-3x" @click="changeTask" ></i><i class="fas fa-arrow-circle-right fa-3x"></i></div></div></div>',
  data: function () {
  	return {
  		saveclass: 'fa-edit',
  		taskText: ''
  	}
  },
  methods: {
    removeTask(count) {
    	console.log(count);
      var jjj=this.$el.getAttribute('data-col');
      if (jjj=='backlog') {
      	app.lists.backlog.splice(count, 1);
      }
      else if (jjj=='inprogress') {
      	app.lists.inprogress.splice(count, 1);
      } else if (jjj=='done') {
      	app.lists.done.splice(count, 1);
      }
      app.saveTask();
    },
   
    changeTask(count) {
    	console.log(this);
    	this.taskText=this.$el.childNodes[2].childNodes[0].value;
    	if ((this.saveclass=='fa-edit')&&(!app.isEdit)) {
    		app.isEdit=!app.isEdit;
    		this.$el.childNodes[2].childNodes[0].classList.toggle('activeText');
    		this.$el.childNodes[2].childNodes[0].removeAttribute('disabled');
    		this.saveclass='fa-save';
    	} else if (this.saveclass=='fa-save') {
    		app.isEdit=!app.isEdit;
    		this.$el.childNodes[2].childNodes[0].classList.toggle('activeText');
    		this.$el.childNodes[2].childNodes[0].setAttribute('disabled', 'true');
    		this.saveclass='fa-edit';
    	} 

    	if (this.$el.getAttribute('data-col')=='backlog') {
    		var add = {
    			id: app.lists.backlog[count].id,
    			text: this.taskText
    		};
    		app.lists.backlog.splice(count, 1, add);
    	}	else if (this.$el.getAttribute('data-col')=='inprogress') {
    		var add = {
    			id: app.lists.inprogress[count].id,
    			text: this.taskText
    		};
    		app.lists.inprogress.splice(count, 1, add);
    	} else if (this.$el.getAttribute('data-col')=='done') {
    		var add = {
    			id: app.lists.done[count].id,
    			text: this.taskText
    		};
    		app.lists.done.splice(count, 1, add);
    	}
    	app.saveTask();
    	}
    }

});

var app = new Vue({
	el: '#app',
	data: {
		isEdit: false,
		can: false,
		newTask: '',
		lists:{
			backlog: [
				{id: '1',
				text: 'Тест 1'}, 
				{id: '2',
				text: 'Тест 2'}, 
			],
			inprogress: [
				{id: '3',
				text: 'Тест 1'}, 
				{id: '4',
				text: 'Тест 2'}, 
			],
			done: [
				{id: '5',
				text: 'Тест 1'}, 
				{id: '6',
				text: 'Тест 2'}, 
			],
		},
		currentId: 1
	},
	methods: {
	addTasks() {
		console.log('TExt: '+this.newTask);
	  if (!this.newTask) { 
		return; 
	  } 
		var nwTask = { 
			id: this.currentId, 
			text: this.newTask
		}; 
		this.currentId++; 
		this.lists.backlog.push(nwTask); 
		this.newTask = ''; 
		this.saveTask(); 
    },
	saveTask() {
		console.log('All saved');
      const parsed = JSON.stringify(this.lists);
      localStorage.setItem('lists', parsed);
      localStorage.setItem('id', this.currentId);
    }},

  mounted() {
    if (localStorage.getItem('lists')) {
      try {
        this.lists = JSON.parse(localStorage.getItem('lists'));
      } catch(e) {
        localStorage.removeItem('lists');
      }

    }
     if (localStorage.getItem('id')) {
      try {
        this.currentId = localStorage.getItem('id');
      } catch(e) {
        localStorage.removeItem('id');
      }

    }
  }
})

